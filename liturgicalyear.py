from datetime import date

import calendar
import rules
import utils


"""
The whole program Generates Liturgical year for Anglican 1662 Book of Common Prayer. 

Note: The liturgical year, also known as the church year or Christian year,
as well as the kalendar, consists of the cycle of liturgical seasons in
Christian churches that determines when feast days,
including celebrations of saints, are to be observed, 
and which portions of Scripture are to be read either 
in an annual cycle or in a cycle of several years.


"""

"""
Class Feast: 
Not really used at this point this is for fixed feast
"""
class Feast:
    idx = None
    name = ""
    red = False 
    
    #moveable

    def __init__(self,idx,name,red):
        self.idx = idx
        self.name = name
        self.red = red
        
    def id(self):        
        return idx
    
    def __str__(self):
        return self.name
    
    def __ge__(self,other):        
        return (self.red and not(other.red))

    def __lt__(self,oher):
        return (not(self.red) and (other.red))

class Day:
    """
    Defines the Feast Day for each day of the Gregorian Calendar
    (Almost every day has one!)
    """    
    season = ""
    week = 0
    day = 0
    fixedid = ""
    fixedfeast = ""
    seasonid =""
    moveablefeast = ""
    ot_morning = ""
    nt_morning = ""
    ot_evening = ""
    nt_evening = ""
    collect_mp = None
    collect_ep = None
    memorial_collect = None
    date = None
    redletter = False
    
    def __init__(self,date):
        self.date = date

    def __str__(self):
        if(not self.moveablefeast == ""):
            return f"{self.date}:{self.moveablefeast},{self.fixedfeast}"
        else:
            return f"{self.date}:{self.fixedfeast}"

    """
    Calcuates an unique id for each day of the year
    """
    def id(self):
        if(len(self.seasonid) == 0):
            if(not self.moveablefeast == ""):
                self.seasonid = f"{self.season}:{self.week}-{self.day}"
            else:
                self.seasonid = ""
        return self.seasonid 

class LiturgicalYear:

    """
    The only input you need is the year you want the calendar will be generated 
    """
    def __init__(self,year):
        self.year = year
        #self.isLeap = calendar.isleap(year) #leap year
        self.days = utils.generate_dates(year) #Leap or not Leap
        self.easter = utils.calc_easter(year) #Easter Changes every year

        self.calendar  = [Day(self.days[i]) for i in range(len(self.days))] #the calendar itself

    def insert(self):
        # A church year starts with advent (first Sunday of December)
        adventdate = rules.advent(self.year,self.calendar)

        #then christmas 
        rules.christmas(self.year,self.calendar)

        #I want to do lent first to calculate epiphany
        lentdate = rules.lent(self.year,self.calendar)

        #epiphany
        rules.epiphany(self.year,self.calendar,lentdate)

        #lent
        rules.passion(self.year,self.calendar)

        #easter
        rules.easter(self.year,self.calendar)

        #50 days after easter 
        rules.ascension(self.year,self.calendar)

        #Pentecost
        rules.whitsunday(self.year,self.calendar)

        #Trinity and after
        rules.trinity(self.year,self.calendar,adventdate)


        """
        TODO:
        Add rules for fixed feasts 
        (for most fixed feasts the days don't change unless they clash with Holy Week and Easter)
        
        """

    def getDate(self,day):
        """
        TODO: get the feast corresponding to day
        """
        pass

    
        

