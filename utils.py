
from datetime import timedelta, date
import constants
import inflect
p = inflect.engine()


"""
utiluty functions
"""
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

"""
returns all days in a year
"""
def generate_dates(year):
    start_date = date(year, 1, 1)
    end_date = date(year+1, 1, 1)
    return [single_date for single_date in daterange(start_date, end_date)]

def calc_easter(year):
    "Returns Easter as a date object."
    a = year % 19
    b = year // 100
    c = year % 100
    d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
    e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
    f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114
    month = f // 31
    day = f % 31 + 1    
    return date(year, month, day)




def index(day):
    return day.timetuple().tm_yday - 1

"""
Converts 1 to First, 2 to Second 
"""    
def num2ordinal(n):
    return p.number_to_words(p.ordinal(n)).capitalize()

def nth_weekday(the_date, nth_week, week_day):
    temp = the_date.replace(day=1)
    adj = (week_day - temp.weekday()) % 7
    temp += timedelta(days=adj)
    temp += timedelta(weeks=nth_week-1)
    return temp
