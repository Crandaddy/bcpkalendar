

weekDays = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")


#CODENAME FOR Liturgicalseason
codenames = {
    'ADVENT':"adv",
    'CHRISTMAS':"nativ",
    'EPIPHANY':"epip",
    'SHROVE':"shrove", 
    'LENT':"lent",
    'PASSION':"passion",
    'EASTER':"paschal",
    'ASCENSION':"asc",
    'WHITSUN':"whitsun",
    'TRINITY':"trin",
}

FORMAT = "season:week-day"

codenames_rev = {
    "adv":'Advent',
    'nativ':'Christmas',
    'epip':'Epiphany',
    'lent':'Lent',
    'paschal':'Easter',
    'asc':"asc",
    'whitsun':"Whitsunday",
    'trinity':"Trin",
}

ADVENT = {"sundays":"Sunday of Advent","weekdays":"Week of Advent","eve":"Christmas Eve"}

SHROVE = {
    "sept":"SEPTUAGESIMA",
    "sexa":"SEXAGESIMA",
    "quin":"QUINQUAGESIMA",
    "mon":"Shrove Monday",
    "tues":"Shrove Tuesday"
}

LENT = {
    "passionsun":"Passion Sunday",
    "palmsun":"Palm Sunday"   
}


PASSION = ("Palm Sunday","Holy Monday","Holy Tuseday","Holy Wednesday","Maundy Thursday","Good Friday","Easter Even")

EASTER ={"resurrection":"Easter Sunday","low":"Low Sunday"}

ASCENSION  ={"asc":"Ascension Day"}

WHITSUN ={"whit":"Whitsunday"}

TRINITY ={"trin":"Trinity Sunday","sundays":"Sunday after Trinity","weekdays":"Week of Trinity"}

CHRISTMAS_FIXED = ("Christmas Day","St. Stephen's Day","St John Evangelist","Holy Innocents","Feast of Saint Thomas Becket","Holy Name of Jesus")

CHRISTMAS_VARIABLE = {"sundays":"Sunday after Christmas","weekdays":"Week of Christmas"}

EPIPHANY_FIXED = "Epiphany"
EPIPHANY_VARIABLE = {"sundays":"Sunday after Epiphany","weekdays":"Week of Epiphany"}


#NAMES
SEPTUAGESIMA="sept"
SEXAGESIMA="sexa"
QUINQUAGESIMA="quin"
PALMSUNDAY="Palm Sunday"

#LENGTH
CHRISTMASTIDE_DAYS = 12
