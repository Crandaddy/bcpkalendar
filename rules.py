import utils
import constants
from datetime import datetime,timedelta
import calendar

weekDays = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")


def adjust(day,week):
    day = day + 1
    if(day % 7) == 0:
        day = 0
        week = week + 1
    return day,week


"""
For Easter, Lent and Trinity Sunday where days changes year to year
"""

def setid(calendar,index,season,week,day,feast,redletter = False):
    calendar[index].season = season
    calendar[index].week = week
    calendar[index].day = day
    calendar[index].seasonid = f"{season}:{week}-{day}"
    calendar[index].moveablefeast = feast


"""
For Christmas and Epiphany etc. Feasts where days are fixed
"""
def setfixedid(calendar,index,season,week,day,feast,redletter = False):
    calendar[index].season = season
    calendar[index].week = week
    calendar[index].day = day
    calendar[index].seasonid = f"{season}:{week}-{day}"
    calendar[index].fixedfeast = feast
    calendar[index].redletter = redletter



    
def lent(year,calendar):
    easter = utils.calc_easter(year)
    easter_idx = utils.index(easter)

    #The start of lent is 46 days before Easter Sunday 
    ash_wed = utils.index(easter-timedelta(46))

    #set up week and day
    week=1
    day=2

    #there are a lot of constants and utils
    season = constants.codenames['LENT']
    week = week
    day = day

    #ash wednesday is constant
    feast = "Ash Wednesday"
    setid(calendar,ash_wed,season,week,day,feast,redletter=True)

    #a for loop does the job filling up the rest 
    for i in range(ash_wed+1,easter_idx-7):
        day,week = adjust(day,week)  
        if(i == easter_idx-7):
            feast = "Palm Sunday"
            redletter = True #the red letter day baiscally means it is important.
        elif(day % 7 == 0):
            feast = f"{utils.num2ordinal(week - 1)} Sunday of Lent"         # sundays of lent   
        elif(i == easter_idx-13):
            feast = "Passion Sunday"
            redletter = True
        else:
            feast = "" #otherwise they are no name
            redletter = False
            #feast = f"{weekDays[day]} in {utils.num2ordinal(week)} Week of Lent"
        setid(calendar,i,season,week,day,feast,redletter)
        redletter = False
            

    #prelent (9 weeks before eawswter)
    week = 0
    day = 0
    name = list(constants.SHROVE.items())
    print(name)
    for i in range(easter_idx-63,easter_idx-46):   
        #calendar[i].season = name[week][0]
        season = constants.codenames['SHROVE'] # A lot of this is hard coded in constants
        week = week
        day = day
        if(day == 6):
            feast = f"{name[week][1]}"  
        elif(i == easter_idx-48):
            feast = f"{name[3][1]}"
        elif(i == easter_idx-47):
            feast = f"{name[4][1]}"            
        else:
            # feast = f"{weekDays[day]} after {name[week-1][1]}"
            feast = ""
        setid(calendar,i,season,week,day,feast)

        day,week = adjust(day,week) #roll over to a new week
           
    return easter_idx-63
            
    #5 weeks
            
    #holy week!
def passion(year,calendar):
    easter = utils.calc_easter(year)
    easter_idx = utils.index(easter)
    week = 0
    day = 0

    #a lot more pushing constants
    for i in range(easter_idx-7,easter_idx):
        #calendar[i].season = name[week][0]
        season = constants.codenames['PASSION']
        week = week
        day = day
        feast = constants.PASSION[day]

        if(i in range(easter_idx-2,easter_idx)):
           redletter = True
        else:
           redletter = False
           
        setid(calendar,i,season,week,day,feast,redletter)
        day = day + 1
    pass

def easter(year,calendar):
    easter = utils.calc_easter(year)
    easter_idx = utils.index(easter)
    week = 0
    day = 0

    # Easter 
    for i in range(easter_idx,easter_idx+39):
        #calendar[i].season = name[week][0]
        season = constants.codenames['EASTER']

        if(i == easter_idx):
            feast = constants.EASTER['resurrection']
        elif(i == easter_idx+7):
            feast = constants.EASTER['low']
        elif((i-easter_idx) % 7 == 0):
            feast = f"{utils.num2ordinal(week+1)} Sunday of Easter" 
        else:
            feast = ""
            #feast = f"{weekDays[day]} in {utils.num2ordinal(week)} Week of Easter"
        
        setid(calendar,i,season,week,day,feast)

        day,week = adjust(day,week)
        
    pass

# Acsensiontide is not long
def ascension(year,calendar):
    easter = utils.calc_easter(year)
    easter_idx = utils.index(easter)
    week = 0
    day = 0
    
    for i in range(easter_idx+39,easter_idx+49):
        season = constants.codenames['ASCENSION']

        if(i == easter_idx+39):
            feast = constants.ASCENSION['asc']
        elif((i-easter_idx) % 7 == 0):
            feast = f"{utils.num2ordinal(week+1)} {constants.weekDays[6]} after {constants.ASCENSION['asc']}"             
        else:
            feast = ""
            feast = f"{weekDays[day-4]} in {utils.num2ordinal(week+1)} Week of Ascension"
            
        setid(calendar,i,season,week,day,feast)
        day,week = adjust(day,week)
        
    pass


"""
Basically the same deal as the rest
"""
def whitsunday(year,calendar):
    easter = utils.calc_easter(year)
    easter_idx = utils.index(easter)
    week = 1
    day = 0
    start = easter_idx+49
    
    for i in range(start,easter_idx+56):
        #calendar[i].season = name[week][0]
        season = constants.codenames['WHITSUN']

        if(i == start):
            feast = constants.WHITSUN['whit']
        else:
            feast = ""
            # feast = f"{weekDays[day+6]} after Whitsunday"
        
        setid(calendar,i,season,week,day,feast)
        day,week = adjust(day,week)
    pass

def trinity(year,calendar,advent_idx):
    easter = utils.calc_easter(year)
    easter_idx = utils.index(easter)
    week = 0
    day = 0
    season = constants.codenames['TRINITY']
    for i in range(easter_idx+56,advent_idx):
        #calendar[i].season = name[week][0]


        if(i == easter_idx+56):
            feast = constants.TRINITY['trin']
        elif((i-easter_idx) % 7 == 0):
            feast = f"{utils.num2ordinal(week)} {constants.TRINITY['sundays']}"             
        else:
            feast = ""
            #feast = f"{weekDays[day-1]} in {utils.num2ordinal(week+1)} {constants.TRINITY['weekdays']}"
        
        setid(calendar,i,season,week,day,feast)

        day,week = adjust(day,week)    
    pass

def epiphany(year,calendar,shrove_idx):
    epiphany_idx = 5
    print(f"epiphany {epiphany_idx}")
    week = 1
    day = datetime(year=year,month=1,day=6).weekday()
    season = constants.codenames['EPIPHANY']
    print(epiphany_idx)
    print(shrove_idx)
    for i in range(epiphany_idx,shrove_idx):
        if(i == epiphany_idx):
            feast = constants.EPIPHANY_FIXED
            setfixedid(calendar,i,season,week,day,feast,redletter=True)            
        elif(day == 6):
            feast = f"{utils.num2ordinal(week)} {constants.EPIPHANY_VARIABLE['sundays']}"
            setid(calendar,i,season,week,day,feast)                        
        else:
            feast = ""
            setid(calendar,i,season,week,day,feast)
        day,week = adjust(day,week)    
    pass


def advent(year,kalendar):
    first_sunday= (utils.nth_weekday(datetime(year=year,month=12,day=1),1,6)).date()
    advent_idx = utils.index(first_sunday)
    christmas_idx  = utils.index(datetime(year=year,month=12,day=25))
    week = 0
    day = 0
    season = constants.codenames['EPIPHANY']
    
    for i in range(advent_idx,christmas_idx):
        if((i - advent_idx) % 7 == 0):
            feast = f"{utils.num2ordinal(week+1)} {constants.ADVENT['sundays']}"
            setid(kalendar,i,season,week,day,feast)
        elif(i == advent_idx):
            feast = constants.EPIPHANY_FIXED[0]
            setfixedid(kalendar,i,season,week,day,feast,redletter=True)
        else:
            feast = ""
            #feast = f"{weekDays[day-1]} in {utils.num2ordinal(week+1)} {constants.ADVENT['weekdays']}"
        
            setid(kalendar,i,season,week,day,feast)

        day,week = adjust(day,week)
    print(advent_idx)
    return advent_idx
    
def christmas(year,kalendar):
    week = 1
    day = 0
    print(day)
    start = utils.index(datetime(year=year,month=12,day=25))
    index = int(start)
    season = constants.codenames['CHRISTMAS']
    for i in range(constants.CHRISTMASTIDE_DAYS):
        date = datetime(year=year,month=12,day=25)
        if(i == 7):
            feast = constants.CHRISTMAS_FIXED[len(constants.CHRISTMAS_FIXED)-1] 
            setfixedid(kalendar,0,season=season,week=week,day=day,feast=feast)    
        elif(i < 4): #fixed feast
            feast = constants.CHRISTMAS_FIXED[i]
            setfixedid(kalendar,start+i,season=season,week=week,day=day,feast=feast)            
        else:
            print("entering loop")
            if(date.weekday() == 6):
                feast = f"{utils.num2ordinal(week)} Sunday after Christmas"
            else:
                feast = ""
                #feast = f"{weekDays[day]} in {utils.num2ordinal(week)} {constants.CHRISTMAS_VARIABLE['weekdays']}"
            print(index)
            setid(kalendar,index,season,week,day,feast)

        
        #at the end of the year do it
        isLeap = calendar.isleap(year)
        if(isLeap and index == 365):
            date = datetime(year=year,month=1,day=1)
            index = 0
        elif(not(isLeap) and index == 364):
            date = datetime(year=year,month=1,day=1)
            index = 0
        else:
            index = index + 1
        day,week = adjust(day,week)
                
        
        
